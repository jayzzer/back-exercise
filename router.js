const express = require('express');
const router = express.Router();

const mainController = require('./app/controllers/main');

router.get('/', mainController.getMainPage);

router.post('/user', mainController.addNewUser);
router.get('/user/:email', mainController.getUserPage);

module.exports = router;