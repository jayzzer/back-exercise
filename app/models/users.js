const level = require('level');

exports.getAllUsers = () => {
    return new Promise( (resolve, reject) => {
        const usersDB = level('users', { valueEncoding: 'json' });

        let users = [];
        usersDB.createReadStream()
            .on('data', user => {
                let parsedUser = user.value;
                parsedUser.email = user.key;

                users.push(parsedUser);
            })
            .on('end', async () => {
                resolve(users);

                await usersDB.close();
            });
    } );
}

exports.addNewUser = async userData => {
    const usersDB = level('users', { valueEncoding: 'json' });
    
    const userKey = userData.email;
    const userValue = {
        name: userData.name,
        surname: userData.surname,
        age: userData.age
    };

    await usersDB.put(userKey, userValue);

    await usersDB.close();
}

exports.getUser = async userEmail => {
    const usersDB = level('users', { valueEncoding: 'json' });

    let foundUser = await usersDB.get(userEmail);
    foundUser.email = userEmail;

    await usersDB.close();
    return foundUser;
}