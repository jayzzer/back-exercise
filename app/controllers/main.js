const Users = require('./../models/users');

exports.getMainPage = async (req, res) => {
    const allUsers = await Users.getAllUsers();

    res.render('index.html', {
        users: allUsers
    });
}

exports.addNewUser = async (req, res) => {
    const newUserData = {
        name: req.body.name,
        surname: req.body.surname,
        age: req.body.age,
        email: req.body.email
    };

    await Users.addNewUser(newUserData);

    res.redirect('/');
}

exports.getUserPage = async (req, res) => {
    const userEmail = req.params.email;

    const user = await Users.getUser(userEmail);

    res.render('user_page.html', {
        user
    });
}