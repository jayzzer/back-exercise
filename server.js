const config = require('./config.js');

const express = require('express');
const nunjucks = require('nunjucks');
const bodyParser = require('body-parser');
const app = express();

app.use(
    express.static(__dirname + '/src'),
    bodyParser(),
    bodyParser.json(),
    bodyParser.urlencoded({ extended: true })
);

nunjucks.configure(__dirname + '/src/view', {
    autoescape: true,
    cache: false,
    express: app
});

app.use('/', require('./router'));

app.listen(config.port);

console.log('Server started on localhost:' + config.port);